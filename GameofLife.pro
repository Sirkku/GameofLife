#-------------------------------------------------
#
# Project created by QtCreator 2016-06-14T23:35:05
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GameofLife
TEMPLATE = app

VPATH += ./src \
         ./forms

SOURCES  += main.cpp\
            Weight.cpp \
            legacy.cpp \
            mainwindow.cpp \
            settingswindow.cpp \
    src/golsimulation.cpp

HEADERS  += mainwindow.h \
            Weight.h \
            Field.h \
            settingswindow.h \
    src/golsimulation.h \
    src/utility.h

FORMS    += mainwindow.ui \
            settingswindow.ui

CONFIG += c++14

#experimental release settings ...
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -O3

CONFIG(debug, debug|release){
    message("debug -> Defining DO_EXTRA_CHECKS")
    DEFINES += DO_EXTRA_CHECKS
} else {
    message("release")
}
