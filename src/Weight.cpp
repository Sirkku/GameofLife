#include "Weight.h"
#include <math.h>


#include <iostream>
#include <exception>
#include <stdlib.h>
#include <iomanip>

void Weight::cache_function()
{
    if(cache_start != nullptr)
        delete[] cache_start;

    cache_start = new double[_size_x*_size_y];

    // sum all for the distribution value calculation
    double sum = 0.0;
    for (int x = 0; x < _size_x; x++)
        for (int y = 0; y < _size_y; y++)
            sum += _get_value(x - _radius, y - _radius);

    // generate cache.
    for (int x = 0; x < _size_x; x++)
        for (int y = 0; y < _size_y; y++)
            cache_start[x + y*_size_x] = _get_value(x - _radius, y - _radius) / sum;

    // place the pointer to the middle
    cache_middle = cache_start + ((1+_size_x) * _radius);
}


Weight::Weight(int radius, double (*weigh_function)(int, int)) :
    _radius(radius),
	_size_x(2 * radius + 1),
    _size_y(2 * radius + 1),
    cache_start(nullptr),
    cache_middle(nullptr),
    _value_function(weigh_function)
{
    cache_function();
}


///
/// \brief Weight::Cache
/// \param radius Radius of the Square to be cached.
/// \param weight_function Pointer to a function mapping (neighborhood, cell value) to (goal).
/// \param force Do a recache even if nothing would change
///
void Weight::Cache(int radius, double (*weight_function)(int, int), bool force)
{
    if(!force && _radius == radius && _value_function == weight_function)
        return;

    setRadius(radius);
    _value_function = weight_function;

    cache_function();
}


double Weight::get(int x, int y) const {
    //TODO: Is this allowed???
#ifdef DO_EXTRA_CHECKS
    if(abs(x) > _radius || abs(y) > _radius)
        std::cerr << "Weight::get called out of range! X:" << x << " Y:" << y << std::endl;
#endif
    return cache_middle[x + y*_size_x];
}

void Weight::dump_to_console()
{
    std::cout << std::setprecision(3);
    for (int i = -_radius; i <= _radius; i++)
    {
        std::cout << "\n";
        for (int j = -_radius; j <= _radius; j++)
        {
            std::cout << std::to_string(get(j, i)) << " ";
        }
    }
    std::cout << std::endl;
}

int Weight::getRadius()
{
    return _radius;
}

double Weight::_get_value(int x, int y) const {
    if(_value_function == nullptr) {
        const double weight_gauss_parameter = -0.125;  // Attention: Negative factor !
        return exp(weight_gauss_parameter*(x*x + y*y));  // Gaußglocke
    } else {
        return _value_function(x, y);
    }
}

Weight::~Weight() {
    delete[] cache_start;
}

void Weight::setRadius(int _r)
{
    _radius = _r;
    _size_x = (2 * _radius + 1);
    _size_y = (2 * _radius + 1);
}
