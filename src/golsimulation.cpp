#include "golsimulation.h"
#include "src/utility.h"
#include "math.h"
#include <random>
#include <QColor>


GoLSimulation::GoLSimulation() :
    _weight(0),
    _weight_func(nullptr)
{
    _field.default_val = minimum_cell_value;
    _field_temp.default_val = minimum_cell_value;
}


GoLSimulation::~GoLSimulation()
{

}

void GoLSimulation::reset()
{
    _time_spent = 0.0;
}


void GoLSimulation::setNeighborWeight(unsigned n, double (*func)(int, int))
{
    _weight_func = func;
    _weight.Cache(n, func);
}


void GoLSimulation::setNeighborRange(unsigned n)
{
    _weight.Cache(n, _weight_func);
}


void GoLSimulation::setGoalFunction(double (*func)(double, double))
{
    _goal_func = func;
}


bool GoLSimulation::step(double delta_t)
{
    const int n = _weight.getRadius();
    double partial_sum;
    double goal_value;

#ifdef DO_EXTRA_CHECKS
    _weight.dump_to_console();
#endif

    for (int y = 0; y < _field.getHeight(); y++)
    for (int x = 0; x < _field.getWidth(); x++) {

        partial_sum = 0.0;
        for (int x2 = -n; x2 <= n; x2++)
        for (int y2 = -n; y2 <= n; y2++)
            partial_sum += _field[x + x2][y + y2] * _weight.get(x2, y2);

#ifdef DO_EXTRA_CHECKS
        if(!insideBounds(partial_sum, minimum_cell_value, maximum_cell_value))
            std::cerr << "OOB! @ " << x << ":" << y << std::endl;
#endif

        goal_value = _goal_func(partial_sum, _field[x][y]);
        _field_temp[x][y] = goal_value - ((goal_value - _field[x][y]) * exp(-delta_t));
    }

    _time_spent += delta_t;
    swap(_field, _field_temp);
    return true;
}


double GoLSimulation::getCell(int x, int y)
{
    return _field[x][y];
}


void GoLSimulation::setCell(double val, int x, int y)
{
    if(insideBounds(val, minimum_cell_value, maximum_cell_value))
        _field[x][y] = val;
}


int GoLSimulation::getHeight() const
{
    return _field.getHeight();
}


int GoLSimulation::getWidth() const
{
    return _field.getWidth();
}


double GoLSimulation::getAge() const
{
    return _time_spent;
}


void GoLSimulation::resize(int x, int y)
{
    _field.resize(x, y);
    _field_temp.resize(_field);
}


QImage GoLSimulation::getImage()
{
    QImage img(_field.getWidth(), _field.getHeight(), QImage::Format_RGBA8888);

    for (int y = 0; y < _field.getHeight(); y++)
        for (int x = 0; x < _field.getWidth(); x++) {
            int red_color = static_cast<int>((_field[x][y]+1.0)*127.5);

#ifdef DO_EXTRA_CHECKS
            if (red_color > 255 || red_color < 0)
                std::cerr << "Color component out of bounds" << std::endl;
#endif DO_EXTRA_CHECKS

            img.setPixel(x, y, QColor(red_color, 0, 0).rgba());
        }

    return img;
}


void GoLSimulation::fill_random()
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<double> dis(-1.0f, 1.0f);
    for (int y = 0; y < _field.getHeight(); y++)
        for (int x = 0; x < _field.getWidth(); x++)
            _field[x][y] = dis(gen);
}
