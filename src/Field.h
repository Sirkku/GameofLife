#ifndef FIELD_HEADER_GUARD
#define FIELD_HEADER_GUARD
#include <memory>
#include <iostream>

template<typename _T>
class Field;

template<typename _T>
class FieldLineRef;

//TODO: Tests schreiben für diese Klasse:
// normaler Zugriff
// Zugriff außerhalb im dbg, rel
// Zu großer Bereich
// -> dann zugriff
// swap testen

//TODO: Fix Behavious of LineRef is Field is unexpectedly deleted
template<typename _T>
class Field {
    class LineRef {
        Field *_parent;
        int _row;
    public:
        LineRef(Field *parent, int row);
        _T& operator[](int line);
    };

    friend class LineRef;

    void dealloc_data();
    void alloc_data(int x, int y);

    int max_x, max_y;
    _T *_data;
public:
    Field();
    Field(int width, int height);
    ~Field();
    template<typename _T2>
    friend void swap(Field<_T2> &lhs, Field<_T2> &rhs);
    void resize(int new_x, int new_y);
    void resize(const Field<_T>& _field);
    int getHeight() const;
    int getWidth() const;
    LineRef operator[](int index);

    _T default_val;
};


template<typename _T>
void Field<_T>::alloc_data(int x, int y) {
    if(x*y <= 0)
        _data = nullptr;
    else {
        _data = new _T[x*y];
        max_x = x;
        max_y = y;
    }
}


template<typename _T>
void Field<_T>::dealloc_data() {
    if (_data != nullptr)
        delete[] _data;
}



//TODO: Should <x, y, data> be a lonely structure?
template<typename _T>
void swap(Field<_T> &lhs, Field<_T> &rhs) {
    int swap_x = lhs.max_x, swap_y = lhs.max_y;
    _T *swap_data = lhs._data;

    lhs.max_x = rhs.max_x;
    lhs.max_y = rhs.max_y;
    lhs._data = rhs._data;

    rhs.max_x = swap_x;
    rhs.max_y = swap_y;
    rhs._data = swap_data;
}


template<typename _T>
Field<_T>::Field() :
    max_x(0),
    max_y(0),
    _data(nullptr),
    default_val(0)
{
    // purposely empty
}


template<typename _T>
Field<_T>::Field(int width, int height) :
    max_x(width),
    max_y(height),
    default_val(0)
{
    alloc_data(max_x, max_y);
}


template<typename _T>
Field<_T>::~Field() {
    if (_data != nullptr)
        delete[] _data;
}


template<typename _T>
void Field<_T>::resize(int new_x, int new_y) {
    dealloc_data();
    alloc_data(new_x, new_y);
}

template<typename _T>
void Field<_T>::resize(const Field<_T> &_field) {
    resize(_field.getWidth(), _field.getHeight());
}


template<typename _T>
int Field<_T>::getHeight() const {
    return max_y;
}


template<typename _T>
int Field<_T>::getWidth() const {
    return max_x;
}


template<typename _T>
typename Field<_T>::LineRef Field<_T>::operator[](int row) {
    return Field<_T>::LineRef(this, row);
}


template<typename _T>
Field<_T>::LineRef::LineRef(Field *parent, int row) :
    _parent(parent),
    _row(row) {
}


template<typename _T>
_T& Field<_T>::LineRef::operator[](int line) {
    static _T null(0);
    if (line < _parent->max_y && _row < _parent->max_x && line >= 0 && _row >= 0)
        return _parent->_data[_row + line*_parent->max_x];
    else
        return null = _T(_parent->default_val);
}


#endif
