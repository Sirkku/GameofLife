#ifndef UTILITY_H
#define UTILITY_H


template<typename _T>
bool insideBounds(const _T& val, const _T& lb, const _T& ub) {
    return lb <= val && val <= ub;
}


#endif // UTILITY_H

