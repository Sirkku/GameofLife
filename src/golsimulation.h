#ifndef GOLSIMULATION_H
#define GOLSIMULATION_H
#include <iostream>
#include "Weight.h"
#include "Field.h"
#include <QImage>

constexpr double maximum_cell_value =  1.0;
constexpr double minimum_cell_value = -1.0;

///
/// \brief The GoLSimulation class is the simulational Unit.
///
class GoLSimulation
{
    Field<double> _field, _field_temp;
    Weight _weight;
    double _time_spent;
    double (*_goal_func)(double, double);  // n c
    double (*_weight_func)(int, int);
public:
    GoLSimulation();
    ~GoLSimulation();
    void reset();

    void setNeighborWeight(unsigned n, double (*func)(int, int));
    void setNeighborRange(unsigned n);
    void setGoalFunction(double (*func)(double, double));

    bool step(double delta_t = 1.0);

    double getCell(int x, int y);
    void setCell(double val, int x, int y);
    int getHeight() const;
    int getWidth() const;
    double getAge() const;

    void fill_random();
    void resize(int x, int y);
    QImage getImage();
};

#endif // GOLSIMULATION_H
