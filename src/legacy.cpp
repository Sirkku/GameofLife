/**
 _____  _                  _____        _       _   _     _
|  __ \(_)                |  __ \      | |     (_) (_)   | |
| |  | |_  ___  ___  ___  | |  | | __ _| |_ ___ _   _ ___| |_   _ __  _   _ _ __   _____   _ _ __ ___
| |  | | |/ _ \/ __|/ _ \ | |  | |/ _` | __/ _ \ | | / __| __| | '_ \| | | | '__| |_  / | | | '_ ` _ \
| |__| | |  __/\__ \  __/ | |__| | (_| | ||  __/ | | \__ \ |_  | | | | |_| | |     / /| |_| | | | | | |
|_____/|_|\___||___/\___| |_____/ \__,_|\__\___|_| |_|___/\__| |_| |_|\__,_|_|    /___|\__,_|_| |_| |_|


                          _     _            _     _                   _       _
                         | |   | |          | |   | |                 | |     | |
  __ _ _   _ ___ ___  ___| |__ | | __ _  ___| |__ | |_ ___ _ __     __| | __ _| |
 / _` | | | / __/ __|/ __| '_ \| |/ _` |/ __| '_ \| __/ _ \ '_ \   / _` |/ _` | |
| (_| | |_| \__ \__ \ (__| | | | | (_| | (__| | | | ||  __/ | | | | (_| | (_| |_|
 \__,_|\__,_|___/___/\___|_| |_|_|\__,_|\___|_| |_|\__\___|_| |_|  \__,_|\__,_(_)


**/

/*
#include <iostream>
#include <random>
#include <cstdio>
#include <iomanip>
#include <string>
#include <sstream>

// Header, der mir .BMPs speichern lässt

#include "Weight.h"
#include "Field.h"
using namespace std;

// Größen des Simulationsfeldes
const int max_x = 400;
const int max_y = 400;

// Zweimal das Raster.
// Die nächste Generation wird immer auf field_data2 generiert und dann zurückkopiert auf field_data
//double field_data[max_x][max_y];
//double field_data2[max_x][max_y];

Field<max_x, max_y> field_data, field_data2;

// Macht eine doofe Konsolenausgabe vom aktuellen Stand der Zellen
void printField() {
	cout.precision(5);
	for (int y = 0; y < max_y; y++) {
		for (int x = 0; x < max_x; x++)  {
			cout << field_data[x][y] << " ";
		}
		cout << endl;
	}
}

// Kopiert nach der Generation von der nächsten Generation auf field_data2 die Daten auf field_data
void copyBack() {
	swap<max_x, max_y>(field_data, field_data2);
	// memcpy(field_data, field_data2, max_x*max_y*sizeof(double));
}

// Parameter für die Simulation
const int n = 5;  // Größe der Nachbarschaft

Weight weight(n);

// Gibt den Zustand einer Zelle in Abhängigkeit ihrer Distanz
// Kleinere Wert -> weniger Wirkung
// cell_size: Wert in [-1.0, 1.0], stellt den Zustand einer Zelle dar
// x, y: Abstand entlang der Achse X und Y
//double weightNode(double cell_size, int x, int y) {
//	const double weight_gauss_parameter = 0.125f;
//	return (x == 0 && y == 0) ? 0 : cell_size / exp(weight_gauss_parameter*(x*x + y*y));  // Gaußglocke 
//}

inline double weightNode(double cell_size, int x, int y) {
	return cell_size * weight.get(x, y);
}

// Faktor, der die Summe der Nachbarschaften in ein Intervall von [-1, 1] zwingt.
double total_weight;

// Berechnung von weight: Es wird über die Summation der größtmöglichste Wert rausgesucht
void calculateWeight() {
	total_weight = 0.0;
	for (int x = -n; x <= n; x++)
		for (int y = -n; y <= n; y++)
			total_weight += weightNode(1.0, x, y);
}

// OutOfBounds handling, damit ich nicht drauf achten muss woanders, ob ich außerhalb des Arrays zugreife.
double get(int x, int y) {
	if (x < 0 || y < 0 || x >= max_x || y >= max_y) return 0.0;
	else return field_data[x][y];
}


inline bool insideBounds(double value, const double &lb, const double &ub) {
	return lb <= value && value <= ub;
}


// Gibt den Zielzustand einer Zelle zurück
// val: Nachbarschaftswert

enum EGoal {GoalLife, GoalDeath, GoalUnchanged};

EGoal goalValue(double val) {
	if (val < 0.0) return GoalLife;
	else return GoalDeath;
}


const double change_rate = 0.125;
const double change_factor = exp(-change_rate);

// Generiere die nächste Generation
void generateNext() {
	double partial_sum;
	double goal;

	// Für alle Position
	for (int y = 0; y < max_y; y++)
		for (int x = 0; x < max_x; x++) {
			// Bestimme den Nachbarschaftswert
			partial_sum = 0.0;
			for (int x2 = -n; x2 <= n; x2++)
				for (int y2 = -n; y2 <= n; y2++)
					partial_sum += weightNode(get(x + x2, y + y2), x2, y2);

			// first normalizes into normal range - makes goalValue independent of the weighting Function
			switch (goalValue(partial_sum / total_weight)) {
			case GoalLife:
				field_data2[x][y] = 1.0 - ((1.0 - field_data[x][y]) * change_factor);
				break;
			case GoalDeath:
				field_data2[x][y] = -1.0 - ((-1.0 - field_data[x][y]) * change_factor);
				break;
			case GoalUnchanged:
				break;
			}

			// Newton cooling equation applied onto cell stati
			// Also Zieltemperatur = Zielzustand; Ausgangstemperatur = aktueller Zellzustand
		}
}

// Fülle das Feld mit Zufallswerten zwischen [-1, 1]
void fillRandom() {
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<double> dis(-1.0f, 1.0f);
	for (int y = 0; y < max_y; y++)
		for (int x = 0; x < max_x; x++) 
			field_data[x][y] = dis(gen);
}

// Wandle ein Zellzustand in ein Farbteil
inline int toColor(double value) {
	return static_cast<int>(127.5f*(value + 1.0f));
}

// Speicher den aktuellen Zustand in eine Datei namens "[number].bmp"
void makeImage(int number) {
	std::stringstream filename;
	filename << std::setw(3) << setfill('0') << number << ".bmp";
	bitmap_image image;
	image.setwidth_height(max_x, max_y);

	for (int y = 0; y < max_y; y++) {
		for (int x = 0; x < max_x; x++) {
			image.set_pixel(x, y, toColor(field_data[x][y]), 0, 0);
		}
	}
	image.save_image(filename.str());
}


const int max_generations = 100;
*/
/**
 _____  _                  _____        _       _   _     _
|  __ \(_)                |  __ \      | |     (_) (_)   | |
| |  | |_  ___  ___  ___  | |  | | __ _| |_ ___ _   _ ___| |_   _ __  _   _ _ __   _____   _ _ __ ___
| |  | | |/ _ \/ __|/ _ \ | |  | |/ _` | __/ _ \ | | / __| __| | '_ \| | | | '__| |_  / | | | '_ ` _ \
| |__| | |  __/\__ \  __/ | |__| | (_| | ||  __/ | | \__ \ |_  | | | | |_| | |     / /| |_| | | | | | |
|_____/|_|\___||___/\___| |_____/ \__,_|\__\___|_| |_|___/\__| |_| |_|\__,_|_|    /___|\__,_|_| |_| |_|


                          _     _            _     _                   _       _
                         | |   | |          | |   | |                 | |     | |
  __ _ _   _ ___ ___  ___| |__ | | __ _  ___| |__ | |_ ___ _ __     __| | __ _| |
 / _` | | | / __/ __|/ __| '_ \| |/ _` |/ __| '_ \| __/ _ \ '_ \   / _` |/ _` | |
| (_| | |_| \__ \__ \ (__| | | | | (_| | (__| | | | ||  __/ | | | | (_| | (_| |_|
 \__,_|\__,_|___/___/\___|_| |_|_|\__,_|\___|_| |_|\__\___|_| |_|  \__,_|\__,_(_)


**/
/*
int main() {
	fillRandom();
	calculateWeight();

	// Generiere 100 Generationen und ihre Bilder
	for (int i = 1; i <= max_generations; i++) {
		// Fortschritt in der Konsole ausgeben
		std::cout << std::setw(static_cast<std::streamsize>(floor(log10(max_generations))) + 1) << std::setfill(' ');
		cout << i << '/' << max_generations << std::endl;
		makeImage(i);
		generateNext();
		copyBack();
	}

	return 0;
}
*/
