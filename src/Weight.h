#pragma once

//TODO: Tests schreiben für

/**
 * @brief The Weight class caches a function in a variable sized square
 */
class Weight
{
    /**
     * @brief cache_function Do the actual caching process
     */
    void cache_function();


public:
    Weight(int radius, double (*weight_function)(int, int) = nullptr);
    void Cache(int radius, double (*weight_function)(int, int) = nullptr, bool force = false);
    double get(int x, int y) const;


    void dump_to_console();


    int getRadius();

	~Weight();
protected:
    void setRadius(int _r);
    int _radius; /** cached parameter range **/
    int _size_x; /** cached parameter width **/
    int _size_y; /** cached parameter height **/

    double _get_value(int x, int y) const;

    double *cache_start; /** pointer to cache  **/
    double *cache_middle; /** pointer to cache at (0, 0) **/
    double (*_value_function)(int, int); /** custom weight function **/
private:
    Weight() {}
};
